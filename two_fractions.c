//WAP to find the sum of two fractions.
#include <stdio.h>

int find_num(int x1,int y1,int x2,int y2)
{
    int A;
    A=(x1*y2)+(x2*y1);
    return A;
}

int find_den(int y1,int y2)
{
    int B;
     B=y1*y2;
     return B;
    
}
int find_gcd(int A,int B)
{
    int i,gcd;
    for(i=1; i<=A && i<=B; ++i)
    {
        if(A%i==0 && B%i==0)
        gcd=i;
    }
    return gcd;
    
}

int display(int U, int V)
{
    printf("the adition of two fraction is %d/%d",U,V);
}
int main()
{
    int x1,y1,x2,y2,A,B,U,V,gcd;
    printf("Enter a value for numerator1:");
    scanf("%d",&x1);
    printf("Enter a value for denominator1:");
    scanf("%d",&y1);
    printf("Enter a value for numerator2:");
    scanf("%d",&x2);
    printf("Enter a value for denominator2:");
    scanf("%d",&y2);
    A=find_num( x1,y1,x2,y2);
    B=find_den(y1,y2);
    gcd=find_gcd(A,B);
    U=A/gcd;
    V=B/gcd;
    display(U,V);
    return 0;
}