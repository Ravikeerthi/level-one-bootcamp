//Write a program to add two user input numbers using 4 functions.
#include<stdio.h>

float input()
{
float x;
scanf ("%f",&x);
return x;
}

float find_sum(int a,int b)
{
float sum;
sum=a+b;
return sum;
}

float display(float sum)
{
printf ("the sum is %f",sum);
}

float main()
{
float a,b,sum;
printf ("Enter a value for first number:");
a=input();

printf ("Enter a value for second number:");
b=input();

 sum=find_sum(a,b);

display(sum);
return 0;
}

