//WAP to find the volume of a tromboloid using 4 functions.
#include<stdio.h>

float input()
{
float a;

scanf("%f",&a);
return a;
}

float find_volume(float h,float b,float d)
{
float volume;
volume= 1.0/3.0 *((h*b*d)+(d/b));
return volume;
}

float display(float volume)
{
printf("The volume of a tromboloid is %f",volume);
}

int main() 
{
float h,b,d,volume;

printf("Enter a value for h:");
h=input();

printf("Enter a value for b:");
b=input();

printf("Enter a value for d:");
d=input();

if(b<=0||h<0||d<0)
{
    printf("give another value for h or b or d ");
}
else
{
volume = find_volume(h,b,d);

display(volume);

return 0;
}}